<?
  putHeader();
  if( $job == "register" ){
    include('register.view.php');
  }else if( $_SESSION['username'] == null){
    // if not logged in, show login dialog
    include("login.view.php");
  }else{
    if(!is_null($params["debug"])) echo $params["debug"];
    if($params["err"]!=null) {
      echo "<span class=\"error\">".$params["err"]."</span><br/>";
    }
    $view = $_SESSION["view"];
    include("default.view.php");
  }
  putFooter();
?>
