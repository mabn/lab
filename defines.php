<?
  include('config.php');
//  include_once('class.pagegen.php');
  
  $params = array();
  
  function HtmlSecure() {
    $_GET = array_map('htmlspecialchars' , $_GET);
    $_POST = array_map('htmlspecialchars' , $_POST);
  }
  function createPassword(){
      // *************************
      // Random Password Generator
      // *************************
      $totalChar = Config::passwordLength; // number of chars in the password
      $salt = 'abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789';  // salt to select chars from
      srand((double)microtime()*1000000); // start the random generator
      $password=""; // set the inital variable
      for ($i=0;$i<$totalChar;$i++)  // loop and create password
                  $password = $password . substr ($salt, rand() % strlen($salt), 1);
      return $password;
  }

  function redirectAfterPost(){
    global $params;
    global $pagegen;
    $url = "index.php";
    $lecture = $_REQUEST["lecture"];
    if(!is_null($lecture)) $url .= "?lecture=$lecture";
    header("Location: $url");
    if($params["err"]) $_SESSION["err"]=$params["err"];
    if($params["debug"]) $_SESSION["debug"]=$params["debug"];
    $pagegen->stop();
    $_SESSION["pagegentime"] = $pagegen->gen();
    die;
  }
  function debug($param){
    global $params;
    if(!Config::debug) return;
    if(is_null($params["debug"])) $params["debug"]="";
    $params["debug"] .= "DEBUG: $param<br/>";
  }
?>
