<?
  function getUsedPoints($usrSublectureT,$userId){
    $queryUsedPoints = "select sum(points) from $usrSublectureT where user_id = $userId";
    debug("$queryUsedPoints");
    $result = mysql_query($queryUsedPoints) or die(mysql_error());
    $row = mysql_fetch_array($result, MYSQL_NUM);
    if(!$row) die("error: zrestartuj przegladarke ;)");
    $pointsUsed = $row[0];
    mysql_free_result($result);
    return $pointsUsed;
  }
  function getAvailiblePoints($userId){
    
    return getTotalPoints() - getUsedPoints(Config::dbPrefix . "user_sublecture", $userId);
  }
  function updateHeaderPoints(){
    $userid = $_SESSION["userId"];
    if(!is_null($userid))
      $_SESSION["pointsAvailible"] = getAvailiblePoints($userid)."/".getTotalPoints();
  }
  function getTotalPoints(){
    global $params;
    if(!is_null($params["userTotalPoints"])){
      return $params["userTotalPoints"];
    }
    $userid = $_SESSION["userId"];
    $userT = Config::dbPrefix."users";
    $query = "select lectures_count from $userT where id = $userid";
    debug($query);
    $result = mysql_query($query) or die("error: ".mysql_error());
    $row = mysql_fetch_assoc($result);
    if(! $row) die("error: zrestartuj przegladarke ;)");
    $lecturesCount = $row["lectures_count"];
    mysql_free_result($result);
    $params["userTotalPoints"]=$lecturesCount * Config::pointsPerLecture;
    return $params["userTotalPoints"];
  }
  function getUsedPointsWithoutLecture($usrSublectureT,$sublectureT,$userId,$lectureId){
    $queryUsedPoints = "select sum(points) from $usrSublectureT as us"
      ." join $sublectureT as s on us.sublecture_id = s.id"
      ." where user_id = $userId and s.lecture_id != $lectureId";
    debug("$queryUsedPoints");
    $result = mysql_query($queryUsedPoints) or die(mysql_error());
    $row = mysql_fetch_array($result, MYSQL_NUM);
    if(!$row) die("error: zrestartuj przegl�dark� ;)");
    $pointsUsed = $row[0];
    mysql_free_result($result);
    return $pointsUsed;

  }
  function isSublectureClosed($sublectureid){
    $sublecturesT = Config::dbPrefix."sublectures";
    $lecturesT = Config::dbPrefix."lectures";
    $query = "select s.id from $sublecturesT as s join $lecturesT as l on s.lecture_id = l.id"
      ." where s.id = $sublectureid and l.closed_at IS NOT NULL";
    debug($query);
    $result = mysql_query($query) or die("error: ".mysql_error());
    if(mysql_num_rows($result) == 1) $ret = true;
    else $ret = false;
    mysql_free_result($result);
    return $ret;
  }

  function isUserUnderGroupPlacesLimit($sublecture, $userid){
    $tabUserSubl = Config::dbPrefix . "user_sublecture";
    $tabSublectures = Config::dbPrefix . "sublectures";
    $tabUser = Config::dbPrefix . "users";
    $query = "select places from $tabSublectures where id = $sublecture";
    $result = mysql_query($query) or die("error: ".mysql_error());
    $row = mysql_fetch_array($result, MYSQL_NUM);
    $places = $row[0];
    mysql_free_result($result);
    debug("limit: $places");

    $ret = true;

    $query = "select"
            ." usl.user_id"
            ." from $tabUserSubl as usl join $tabSublectures as sl "
            ." on usl.sublecture_id = sl.id "
            ." join $tabUser as u on usl.user_id = u.id"
            ." where sl.id = $sublecture"
            ." order by usl.fixed desc, usl.points desc, usl.time asc"
            ." limit $places";
    debug("isUserUnderGroupPlacesLimit:". $query);
    $result = mysql_query($query) or die("error: ".mysql_error());
    while ($row = mysql_fetch_array($result, MYSQL_NUM)){
      if($row[0] == $userid) {
        $ret = false;
        break;
      }
    }
    mysql_free_result($result);
    debug("isUserUnderGroupPlacesLimit: $ret");
    return $ret;
  }
  function doSignup(){
    global $params;
    $usrSublectureT = Config::dbPrefix . "user_sublecture";
    $sublecturesT   = Config::dbPrefix . "sublectures";
    $lecture    = mysql_real_escape_string($_REQUEST["lecture"]);
    $sublecture = mysql_real_escape_string($_REQUEST["sublecture"]);
    $points     = mysql_real_escape_string($_REQUEST["points"]);
    

    if(isSublectureClosed($sublecture)){
      $params["err"] = "Zapisy na wybrany termin s� zablokowane";
      return;
    }
    if(! is_numeric($points)) {
      $params["err"] = "Punkty musz� by� liczb� ca�kowit�";
      return;
    }
    $points = intval($points);
    $userId     = mysql_real_escape_string($_SESSION["userId"]);
    if($lecture == null || $sublecture == null || is_null($points)){
      $params["err"] = "doSignup: error";
      return;
    }
    
    // wyliczamy ilo�� wszystkich punkt�w
    $totalPoints = getTotalPoints();
    debug("total:$totalPoints");
    
    // sprawdzamy ile pktow zostalo
    $pointsUsed = getUsedPointsWithoutLecture($usrSublectureT,$sublecturesT,$userId,$lecture);
    if($points < 0) {
      $params["err"] = "Bardzo smieszne...";
      return;
    }
    else if($pointsUsed + $points > $totalPoints){
      $params["err"] = "Nie mo�esz przekroczy� limitu punkt�w, "
      ."kt�ry wynosi " . $totalPoints;
      return;
    }
    
    // sprawdzenie, czy taki sublecture istnieje i nalezy do wybranego lecture
    // TODO
    
    // wypisujemy usera z wybranego przedmiotu
     unsign($userId, $usrSublectureT, $sublecture, $sublecturesT, $lecture);

   // zapisujemy na wybrany sublecture
   $query = "insert into $usrSublectureT (user_id,sublecture_id,points) values "
      ."($userId, $sublecture, $points)";
    debug("$query");
    mysql_query($query) or die(mysql_error());
    

  }
  function unsign($userid, $usrSublectureT, $sublecture, $sublecturesT, $lecture){
    global $params;

    // wypisanie z przedmiotu, na ktory zapisy zostaly zamkniete
    if(isSublectureClosed($sublecture)){
      if(Config::permitUnsignIfBelowPlaces && isUserUnderGroupPlacesLimit($sublecture,$userid)){
        // user can unsign
        debug("user can unsign");
      }else{
        $params["err"] = "Zapisy na wybrany termin s� zablokowane";
        return false;
      }
    }

    $queryUnsign = "delete from $usrSublectureT where user_id = $userid and "
      ." sublecture_id in (select id from $sublecturesT where lecture_id = $lecture)";
    debug("$queryUnsign");
    mysql_query($queryUnsign) or die(mysql_error());
    return true;
  }
  function doUnsign(){
    global $params;
    $usrSublectureT = Config::dbPrefix . "user_sublecture";
    $sublecturesT   = Config::dbPrefix . "sublectures";
    $lecture    = (int)mysql_real_escape_string($_REQUEST["lecture"]);
    $sublecture = (int)mysql_real_escape_string($_REQUEST["sublecture"]);
    unsign($_SESSION["userId"], $usrSublectureT, $sublecture, $sublecturesT, $lecture);
  }
?>
