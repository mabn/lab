<?
  function isUserAdmin(){
    global $params;
    $userid = $_SESSION["userId"];
    $usersT = Config::dbPrefix."users";
    $query = "select id from $usersT where id=$userid and permissions = 'A'" or die("error: ".mysql_error());
    $result = mysql_query($query);
    if(mysql_num_rows($result) == 1){
      $ret = true;
    }else{
      $ret = false;
    }
    $_SESSION["userPermissions"]= ($ret?'A':'U');
    mysql_free_result($result);
    return $ret;
  }
  function doCloseAll(){
    global $params;
    if(!isUserAdmin()){
      $params["err"] = "Nie masz wystarczających uprawnień";
      return;
    }
    $lecturesT = Config::dbPrefix."lectures";
    $query = "update $lecturesT set closed_at = NOW() where closed_at IS NULL";
    debug($query);
    mysql_query($query) or die("error: ".mysql_error());
    debug("affected rows: ".mysql_affected_rows());
  }
  function doOpenClose(){
    global $params;
    $lecturesT = Config::dbPrefix."lectures";
    if(!isUserAdmin()){
      $params["err"] = "Nie masz wystarczających uprawnień";
      return;
    }
    $lecture = intval($_REQUEST["lecture"]);
    $query = "update $lecturesT set closed_at = NOW() where closed_at IS NULL and id = $lecture";
    debug($query);
    mysql_query($query) or die("error: ".mysql_error());
    debug("affected rows: ".mysql_affected_rows());
    if(mysql_affected_rows() == 0){
      $query = "update $lecturesT set closed_at = NULL where closed_at IS NOT NULL and id = $lecture";
      debug($query);
      mysql_query($query) or die("error: ".mysql_error());
      debug("affected rows: ".mysql_affected_rows());
    }
  }
?>
