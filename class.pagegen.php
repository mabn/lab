<?php
    class page_gen {
        //
        // PRIVATE CLASS VARIABLES
        //
        var $_start_time;
        var $_stop_time;
        var $_gen_time;
        
        //
        // USER DEFINED VARIABLES
        //
        var $round_to;
        
        //
        // CLASS CONSTRUCTOR
        //
        function page_gen() {
            if (!isset($this->round_to)) {
                $this->round_to = 4;
            }
        }
        
        //
        // FIGURE OUT THE TIME AT THE BEGINNING OF THE PAGE
        //
        function start() {
            $microstart = explode(' ',microtime());
            $this->_start_time = $microstart[0] + $microstart[1];
        }
        
        //
        // FIGURE OUT THE TIME AT THE END OF THE PAGE
        //
        function stop() {
            $microstop = explode(' ',microtime());
            $this->_stop_time = $microstop[0] + $microstop[1];
        }
        
        //
        // CALCULATE THE DIFFERENCE BETWEEN THE BEGINNNG AND THE END AND RETURN THE VALUE
        //
        function gen() {
            $this->_gen_time = round($this->_stop_time - $this->_start_time,$this->round_to);
            return $this->_gen_time; 
        }
    }
?>
