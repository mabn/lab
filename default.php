<?
  function doDefault(){
    global $params;
    updateHeaderPoints();
    $tabLectures = Config::dbPrefix . "lectures";
    $tabSublectures = Config::dbPrefix . "sublectures";
    $tabUserSubl = Config::dbPrefix . "user_sublecture";
    $tabUser = Config::dbPrefix . "users";
    $query = "select * from $tabLectures";
    $result = mysql_query($query) or die (mysql_error());
    $lectures = array();
    while ($row=mysql_fetch_assoc($result)){
      if($_REQUEST["lecture"] == $row["id"]){
        $currentLecture = array("id" => $row["id"],
                                "name" => $row["name"],
                                "notes" => $row["notes"],
                                "closed_at" => $row["closed_at"]);
        debug("current lecture is closed at ".$row["closed_at"]);
      }
      debug("adding lecture ".$row["id"].", closed at ".$row["closed_at"]);
      $lectures[$row["id"]] = array("id" => $row["id"],
                                    "name" => $row["name"],
                                    "notes" => $row["notes"],
                                    "closed_at" => $row["closed_at"]
                                   );
    }
    $params["lectures"] = $lectures;
    mysql_free_result($result);

    if($currentLecture != null){
      $query = "select * from $tabSublectures where lecture_id=".$currentLecture["id"];
      $result = mysql_query($query) or die (mysql_error());
      $sublectures = array();
      while ($row=mysql_fetch_assoc($result)){
        $sublectures[$row["id"]] = array("name" => $row["name"],
                                         "lecture_id" => $row["lecture_id"],
                                         "places" => $row["places"],
                                         "users" => array());
      }
      $params["sublectures"] = $sublectures;
      mysql_free_result($result);

      $query = "select"
        ." usl.sublecture_id,usl.user_id,usl.points,usl.fixed,usl.time,u.firstname,u.lastname,"
          ."u.nick,u.group,u.year"
        ." from $tabUserSubl as usl join $tabSublectures as sl "
        ." on usl.sublecture_id = sl.id "
        ." join $tabUser as u on usl.user_id = u.id"
        ." where sl.lecture_id = ".$currentLecture["id"]
        ." order by usl.fixed desc, usl.points desc, usl.time asc";
      $result = mysql_query($query)  or die (mysql_error());
      while($row=mysql_fetch_assoc($result)){
        $user = array("id" => $row["user_id"],
                      "points" => $row["points"],
                      "sublecture_id" => $row["sublecture_id"],
                      "fixed" => $row["fixed"],
                      "firstname" => $row["firstname"],
                      "lastname" => $row["lastname"],
                      "nick" => $row["nick"],
                      "group" => $row["group"],
                      "year" => $row["year"]
                      );
        array_push($sublectures[$row["sublecture_id"]]["users"], $user);
      }
      foreach($sublectures as $sublectureId => $data){
        if($data["places"] <= count($data["users"])){
          $min = $data["users"][$data["places"]-1]["points"];
        }else{
          $min = $data["users"][count($data["users"])-1]["points"];
        }
        $sublectures[$sublectureId]["min"] = $min;
      }
      $params["sublectures"] = $sublectures;
      mysql_free_result($result); 


      $query = "select u.id, sum(us.points), u.lectures_count from $tabUser as u join $tabUserSubl as us on u.id = us.user_id group by u.id";
      debug($query);
      $result = mysql_query($query) or die("error: ".mysql_error());
      $userPoints = array();
      while($row = mysql_fetch_array($result)){
        $userPoints[$row[0]] = ($row[2]* Config::pointsPerLecture - $row[1])."/"
          .$row[2] * Config::pointsPerLecture;
      }
      $params["userPoints"] = $userPoints;
      mysql_free_result($result);
    }
  }
?>
