create table `lab_lectures` (
  id int NOT NULL auto_increment PRIMARY KEY,
  name varchar(100) NOT NULL,
  notes varchar(1000)
) ENGINE=InnoDB;

create table lab_sublectures (
  id int NOT NULL auto_increment PRIMARY KEY,
  name varchar(200) NOT NULL,
  lecture_id int NOT NULL,
  places int NOT NULL default 10
) ENGINE=InnoDB;

create table lab_user_sublecture (
  user_id int NOT NULL,
  sublecture_id int NOT NULL,
  points int NOT NULL,
  fixed varchar(1) NOT NULL default 'N'
)ENGINE=InnoDB;

insert into lab_sublectures (name, lecture_id) values ("poniedziałek 11-12:30", 1);
insert into lab_sublectures (name, lecture_id) values ("poniedziałek 12:30-14", 1);
insert into lab_sublectures (name, lecture_id) values ("czwartek 14:00-15:30", 1);
insert into lab_sublectures (name, lecture_id) values ("czwartek 14:00-15:30", 1);
insert into lab_sublectures (name, lecture_id) values ("czwartek 15:30-17:00", 1);
insert into lab_sublectures (name, lecture_id) values ("czwartek 17:00-18:30", 1);
insert into lab_sublectures (name, lecture_id) values ("czwartek 12:30-14:00", 1);
