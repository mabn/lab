<?
  class Config{
    const sessionCookieName = "labCookie";
    const sessionCookiePath = "/lab/";  // remember: with trailing '/'
    const sessionCookieDomain = ".mabn.pl"; // must start with '.'
    const sessionTimeout = "0"; // never
    
    const dbHost = "qqq";
    const dbUsername = "yyy";
    const dbPassword = "xxx";
    const dbDatabase = "zzz";
    const dbPrefix = "lab_";
    const passwordLength = 10;

    const pointsPerLecture = 100;
    const showCompleteListing = true;
    const listPoints = true;
    const debug = false;

    // pozwala wypisac sie z przedmiotu na ktory zapisy sa juz zamkniete,
    // jezeli student nie zmie�ci� si� w limicie miejsc (jest pod kresk�)
    const permitUnsignIfBelowPlaces = true;
  }
?>
