<?
  include ('defines.php');
  
  $dbConn = mysql_connect(Config::dbHost, Config::dbUsername, Config::dbPassword) or die("unable to connect to database");
  mysql_select_db(Config::dbDatabase) or die("database select failed:".mysql_error());
  
  $result = mysql_query("show tables from ".Config::dbDatabase." like '".Config::dbPrefix."%'") 
    or die("query failed:".mysql_error());
  $tables = array();
  while ($row = mysql_fetch_array($result, MYSQL_NUM)) {
      echo "found table '$row[0]'<br/>";
      array_push($tables, $row[0]);
  }
  mysql_free_result($result);
  $prefix = Config::dbPrefix;
$query = "CREATE TABLE IF NOT EXISTS `${prefix}lectures` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `notes` text,
  `closed_at` timestamp NULL default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2;";

mysql_query($query) or die("error: " . mysql_error());

$query = "CREATE TABLE IF NOT EXISTS `${prefix}sublectures` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(200) NOT NULL default '',
  `lecture_id` int(11) NOT NULL default '0',
  `places` int(11) NOT NULL default '10',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2;";

mysql_query($query) or die("error: " . mysql_error());

$query = "CREATE TABLE IF NOT EXISTS `${prefix}user_sublecture` (
  `user_id` int(11) NOT NULL default '0',
  `sublecture_id` int(11) NOT NULL default '0',
  `points` int(11) NOT NULL default '0',
  `fixed` char(1) NOT NULL default 'N',
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin2;";

mysql_query($query) or die("error: " . mysql_error());

$query = "CREATE TABLE IF NOT EXISTS `${prefix}users` (
  `id` int(11) NOT NULL auto_increment,
  `login` varchar(30) NOT NULL default '',
  `password` varchar(30) NOT NULL default '',
  `email` varchar(50) NOT NULL default '',
  `firstname` varchar(20) NOT NULL default '',
  `lastname` varchar(20) NOT NULL default '',
  `group` varchar(20) default NULL,
  `year` varchar(20) default NULL,
  `nick` varchar(40) default NULL,
  `lectures_count` int(11) NOT NULL default '8',
  `permissions` char(1) NOT NULL default 'U',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2 ;";
  
  mysql_query($query) or die("error: " . mysql_error());

$query = "CREATE TABLE IF NOT EXISTS `lab_audit` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) NOT NULL default '0',
  `hidden1` int(11) NOT NULL default '0',
  `referer` varchar(200) default NULL,
  `useragent` varchar(200) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2 AUTO_INCREMENT=99 ;"

  mysql_query($query) or die("error: " . mysql_error());

  mysql_close($dbConn);
  echo "<br/><br/>done";
?>
