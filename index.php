<? 
  include("class.pagegen.php");
  $pagegen = new page_gen();
  $pagegen->round_to = 7;
  $pagegen->start();
  
  ob_start("ob_gzhandler");
  include('header.php');
  include('footer.php');
  include('defines.php');
  include('login.php');
  include('register.php');
  include('default.php');
  include('signup.php');
  include('admin.php');
  include('b.php');
  HtmlSecure();
  $job = "none";
  if($_POST["job"]!=null) $job=$_POST["job"];
  if($_GET["job"]!=null) $job=$_GET["job"];
  session_name(Config::sessionCookieName);
  session_set_cookie_params(Config::sessionTimeout,
      Config::sessionCookiePath,
      Config::sessionCookieDomain);
  session_start();
  if($_SESSION["err"]){
    $params["err"]=$_SESSION["err"];
    unset($_SESSION["err"]);
  }if($_SESSION["debug"]){
    $params["debug"]=$_SESSION["debug"];
    unset($_SESSION["debug"]);
  }
  if($job == "logout"){
    $_SESSION = array();
    if (isset($_COOKIE[session_name()])) {
      setcookie(session_name(), '', time()-42000, Config::sessionCookiePath, Config::sessionCookieDomain);
      session_destroy();
      // i co teraz? jakies refresh stronki...
      header("Location: index.php");
      die;
    }
  }
  $dbConn = mysql_connect(Config::dbHost, Config::dbUsername, Config::dbPassword) 
    or die("mysql: " . mysql_error());
  mysql_select_db(Config::dbDatabase) or die("database select failed: " . mysql_error());
  mysql_query("set names 'latin2'");
  if($job == "login"){
    doLogin();
    redirectAfterPost();
  }else if($job == "register"){
    doRegister();
  }else if($job == "signup"){
    detectBots();
    doSignup();
    redirectAfterPost();
  }else if($job == "unsign"){
    doUnsign();
    redirectAfterPost();
  }else if($job == "profile"){
    $params["err"] = "edycja profilu jeszcze nie jest zrobiona...";
    doDefault();
  }else if($job == "adminCloseAll"){
    doCloseAll();
    redirectAfterPost();
  }else if($job == "adminOpenClose"){
    doOpenClose();
    redirectAfterPost();
  }else if($job == "231726541"){
    //doA();
    die;
  }else{
    doDefault();
  }
  
  include('selectView.php');
  
  mysql_close($dbConn);
?>
